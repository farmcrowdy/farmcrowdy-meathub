import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


String backendUrl = '{YOUR_BACKEND_URL}';
String paystackPublicKey = 'pk_test_74f3eb6362173647ca0c399baf5efda5ba7dc1c1';
String secretKey = 'sk_test_efe0e46a8a21eef111cadea486f68e928c9a81ea';


// ignore: must_be_immutable
class Payment extends StatefulWidget {
   var amount;
   var amountShow;
   String referenceNo;
  Payment({Key key, this.amount, this.amountShow, this.referenceNo}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(amount, amountShow,referenceNo);
}

class _HomePageState extends State<Payment> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();


  var amount;
  var amountShow;
  String referenceNo;

  _HomePageState(this.amount, this.amountShow, this.referenceNo);

  int _radioValue = 0;
  CheckoutMethod _method;
  bool _inProgress = false;
  String _cardNumber;
  String _cvv;
  int _expiryMonth = 0;
  int _expiryYear = 0;
  var email;
  String name;

  // get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      email = prefs.getString('email');
      name = prefs.getString('name');
    });
  }

    @override
  void initState() {
    PaystackPlugin.initialize(publicKey: paystackPublicKey);
    super.initState();
    setState(() {
      _method = CheckoutMethod.card;
    });
      print(amount);
      getToken();

    Timer(Duration(seconds: 1), () {
      setState(() {
        _handleCheckout(context);
      });
    });


  }
// Alert dialog
  _showErrorDialog(
      {String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Payment Failed"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
    );
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//Center(
//  child:
//      Container(
//          margin: EdgeInsets.only(left: 10),
//      height: 100,
//      width: 300,
//      child:
//        new Card(
//       child:
//          Column(
//crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//
//            Text(
//              'Pay   ' + 'N'+ amountShow.toString(),
//            ),
//              SizedBox(
//                height: 20,
//              ),
//              Container(
//                  margin: EdgeInsets.only(left: 10),
//                  height: 25,
//                  child:
//                  FlatButton(
//                      textColor: Colors.white,
//                      child: Text('Pay'),
//                      color: Colors.blue,
//                      shape: RoundedRectangleBorder(
//                        borderRadius: new BorderRadius
//                            .circular(18.0),
//                        //side: BorderSide(color: Colors.red)
//                      ),
//                      onPressed: () {
//                        //_startAfreshCharge();
//                        _handleCheckout(context);
//                      }),
//              ),
//        ],
//          ),
//
//      ),
//
//)
//),
        ],
      )

    );
  }

  void _handleRadioValueChanged(int value) =>
      setState(() => _radioValue = value);

  _handleCheckout(BuildContext context) async {
    if (_method == null) {
      _showMessage('Select checkout method first');
      return;
    }

    if (_method != CheckoutMethod.card && _isLocal) {
      _showMessage('Select server initialization method at the top');
      return;
    }
    setState(() => _inProgress = true);

    Charge charge = Charge()
      ..amount = amount // In base currency
      ..email = email
      ..reference = _getReference()
      ..putCustomField('Charged From $name', 'MeatHub')
      ..card = _getCardFromUI();

    if (!_isLocal) {
      var accessCode = await _fetchAccessCodeFrmServer(_getReference());
      charge.accessCode = accessCode;
    } else {
      charge.reference = _getReference();
    }

    try {
      CheckoutResponse response = await PaystackPlugin.checkout(
        context,
        method: _method,
        charge: charge,
        fullscreen: true,
        logo: MyLogo(),
      );
      print('Response = $response');
      setState(() => _inProgress = false);
      _updateStatus(response.reference, '$response');
    } catch (e) {
      setState(() => _inProgress = false);
      _showMessage("Check console for error");
      rethrow;
    }
  }

  _startAfreshCharge() async {
    //_formKey.currentState.save();

    Charge charge = Charge();
    charge.card = _getCardFromUI();

    setState(() => _inProgress = true);

    if (_isLocal) {
      // Set transaction params directly in app (note that these params
      // are only used if an access_code is not set. In debug mode,
      // setting them after setting an access code would throw an exception

      charge
        ..amount = amount// In base currency
        ..email = email
        ..reference = _getReference()
        ..putCustomField('Charged From $name', 'MeatHub');
      _chargeCard(charge);
      print(charge);
    } else {
      // Perform transaction/initialize on Paystack server to get an access code
      // documentation: https://developers.paystack.co/reference#initialize-a-transaction
      charge.accessCode = await _fetchAccessCodeFrmServer(_getReference());
      _chargeCard(charge);
      print(charge);
    }
  }

  _chargeCard(Charge charge) {
    // This is called only before requesting OTP
    // Save reference so you may send to server if error occurs with OTP
    handleBeforeValidate(Transaction transaction) {
      _updateStatus(transaction.reference, 'validating...');
    }

    handleOnError(Object e, Transaction transaction) {
      // If an access code has expired, simply ask your server for a new one
      // and restart the charge instead of displaying error
      if (e is ExpiredAccessCodeException) {
        _startAfreshCharge();
        _chargeCard(charge);
        return;
      }

      if (transaction.reference != null) {
        _verifyOnServer(transaction.reference);
      } else {
        setState(() => _inProgress = false);
        _updateStatus(transaction.reference, e.toString());
        _showErrorDialog(message: e.toString() );
      }
    }

    // This is called only after transaction is successful
    handleOnSuccess(Transaction transaction) {
      _verifyOnServer(transaction.reference);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => CustomDialog(
          title: "Success",
          description:
          'Payment Successful,\n check your email for the receipt',
          buttonText: "Okay",
        ),
      );
    }
    {


    }

    PaystackPlugin.chargeCard(context,
        charge: charge,
        beforeValidate: (transaction) => handleBeforeValidate(transaction),
        onSuccess: (transaction) => handleOnSuccess(transaction),
        onError: (error, transaction) => handleOnError(error, transaction));
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }
    return '$referenceNo';

  }

  PaymentCard _getCardFromUI() {
    // Using just the must-required parameters.
    return PaymentCard(
      number: _cardNumber,
      cvc: _cvv,
      expiryMonth: _expiryMonth,
      expiryYear: _expiryYear,
    );

  }

  Widget _getPlatformButton(String string, Function() function) {
    // is still in progress
    Widget widget;
    if (Platform.isIOS) {
      widget = new CupertinoButton(
        onPressed: function,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        color: CupertinoColors.activeBlue,
        child: new Text(
          string,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      );
    } else {
      widget = new RaisedButton(
        onPressed: function,
        color: Colors.blueAccent,
        textColor: Colors.white,
        padding: const EdgeInsets.symmetric(vertical: 13.0, horizontal: 10.0),
        child: new Text(
          string.toUpperCase(),
          style: const TextStyle(fontSize: 17.0),
        ),
      );
    }
    return widget;
  }

  Future<String> _fetchAccessCodeFrmServer(String reference) async {
    String url = '$backendUrl/new-access-code';
    String accessCode;
    try {
      print("Access code url = $url");
      http.Response response = await http.get(url);
      accessCode = response.body;
      print('Response for access code = $accessCode');
    } catch (e) {
      setState(() => _inProgress = false);
      _updateStatus(
          reference,
          'There was a problem getting a new access code form'
          ' the backend: $e');
    }

    return accessCode;
  }

  void _verifyOnServer(String reference) async {
    _updateStatus(reference, 'Verifying...');
    String url = 'https://api.paystack.co/transaction/verify/$reference';
    try {
      http.Response response = await http.get(url,headers: {'Authorization':'Bearer sk_test_efe0e46a8a21eef111cadea486f68e928c9a81ea'});
      var body = response.body;
      _updateStatus(reference, body);
    } catch (e) {
      _updateStatus(
          reference,
          'There was a problem verifying %s on the backend: '
          '$reference $e');
    }
    setState(() => _inProgress = false);
  }

  _updateStatus(String reference, String message) {



    _showMessage('Reference: $reference \n\ Response: $message',
        const Duration(seconds: 7));
  }



  _showMessage(String message,
      [Duration duration = const Duration(seconds: 4)]) {
    print('msg$message');
    Navigator.pushReplacementNamed(context, "/BottomNav");

  }

  bool get _isLocal => _radioValue == 0;
}

var banks = ['Selectable', 'Bank', 'Card'];

CheckoutMethod _parseStringToMethod(String string) {
  CheckoutMethod method = CheckoutMethod.selectable;
  switch (string) {
    case 'Bank':
      method = CheckoutMethod.bank;
      break;
    case 'Card':
      method = CheckoutMethod.card;
      break;
  }
  return method;
}

class MyLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
height: 60,
      width: 130,
      child:
      Image.asset('images/logo.png', ),
    );
  }
}

const Color green = const Color(0xFF3db76d);
const Color lightBlue = const Color(0xFF34a5db);
const Color navyBlue = const Color(0xFF031b33);


class CustomDialog extends StatelessWidget {
  final String title, description, buttonText;
  final Image image;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,

      backgroundColor: Colors.transparent,
      child:
     Stack(
    children: <Widget>[
    Container(
    padding: EdgeInsets.only(
    top: Consts.avatarRadius + Consts.padding,
    bottom: Consts.padding,
    left: Consts.padding,
    right: Consts.padding,
    ),
    margin: EdgeInsets.only(top: Consts.avatarRadius),
    decoration: new BoxDecoration(
    color: Colors.white,
    shape: BoxShape.rectangle,
    borderRadius: BorderRadius.circular(Consts.padding),
    boxShadow: [
    BoxShadow(
    color: Colors.black26,
    blurRadius: 10.0,
    offset: const Offset(0.0, 10.0),
    ),
    ],
    ),
    child: Column(
    mainAxisSize: MainAxisSize.min, // To make the card compact
    children: <Widget>[
    Text(
    title,
    style: TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w700,
    ),
    ),
    SizedBox(height: 16.0),
    Text(
    description,
    textAlign: TextAlign.center,
    style: TextStyle(
    fontSize: 16.0,
    ),
    ),
    SizedBox(height: 24.0),
    Align(
    alignment: Alignment.bottomRight,
    child: FlatButton(
    onPressed: () {
    Navigator.of(context).pop(); // To close the dialog
    },
    child:
        GestureDetector(
          onTap:  () => Navigator.pushReplacementNamed(context, "/BottomNav"),
          child:
    Text(buttonText),
        ),
    ),
    ),
    ],
    ),
    ),
    Positioned(
    left: Consts.padding,
    right: Consts.padding,
    child: CircleAvatar(
    backgroundColor: Colors.blueAccent,
    backgroundImage: AssetImage('images/mark.png'),
    radius: Consts.avatarRadius,
    ),
    ),
    //...bottom card part,
    //...top circlular image part,
    ],
    ),
    );
  }
}

class Consts {
  Consts._();
  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}