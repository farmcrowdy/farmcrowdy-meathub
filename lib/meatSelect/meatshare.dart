import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/cutPojo.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/basket/cart.dart';
import 'package:meathub/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:cached_network_image/cached_network_image.dart';


class MeatShareOrder extends StatefulWidget {
  final int id;
  final String title;
  final String image;
  MeatShareOrder({Key key, this.id, this.title, this.image}) : super(key: key);
  @override
  _AlmostThere createState() => _AlmostThere(id, title, image);
}

class _AlmostThere extends State<MeatShareOrder> {
  int id;
  String title;
  String image;
  _AlmostThere(this.id, this.title, this.image);
  int _quantity = 0;
  String description;
  var price = 0;
  String location;
  var locations = List();
  var data;
  String message;
  Map<String, dynamic> responseData;
  Map<String, dynamic> resData;
  var token;
  var userId;
  var amount = 0;
  var basketNum;
  var cut;
  String cutSelected;

  void add() {
    setState(() {
      _quantity++;
    });
  }
  void minus() {
    setState(() {
      if (_quantity != 0)
        _quantity--;
    });
  }

// get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      userId = prefs.getString('userId');
    });
  }
  Future<List<Cut>> fetchCut() async {
    final response = await http
        .get( baseUrl + 'package/$id', headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      resData = json.decode(response.body);
      cut = resData["data"]['cut_sizes'];
      print(cut);
      final items = cut.cast<Map<String, dynamic>>();
      List<Cut> listOfUsers = items.map<Cut>((json) {
        return Cut.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      resData = json.decode(response.body);
      var er = resData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }
  void _valChange(String value) {
    setState(() {
      cutSelected = value;
      print('dd$cutSelected');
    });
  }

  fetchData() async {
    final response = await http.get(
      baseUrl + 'basket/size',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      basketNum = responseData['data'];
      setState(() {
        basketSize = basketNum;
      });
      print(basketSize);
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // get all the cart data
  void fetchCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      subTotal = responseData['data']['subtotal'];
      total = responseData['data']['total'];
      containerCharge = responseData['data']['container_charges'];
      vat = responseData['data']['vat'];
      basketSize = responseData['data']['basket_size'];

      Navigator.pushReplacementNamed(context, '/BottomNav');
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      Navigator.pushReplacementNamed(context, '/BottomNav');
      throw Exception('Failed to load internet');
    }
  }

  // get  the basket data
  fetchBasket() async {
    final response = await http.get(
      baseUrl + 'package/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      setState(() {
        description = data['description'];
        price = data['price'];
        //cut = data['cut_sizes'];
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }



  //Api to add to the basket

  createOrder( BuildContext context, int type, AnimationController controller)async {
    controller.forward();
    var body = {
      'user_id':userId.toString(),
      'package_id': id.toString(),
      'quantity': _quantity.toString(),
      'weight':'1',
      'status_id': '3',
      'cut_size': cutSelected
    };

    var header =
    {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    print(body);
    print(header);
    final response = await http.post(
        baseUrl + 'order/create',
        headers:header, body: body);
    if (response.statusCode == 200) {
      controller.reverse();
      fetchCart();
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(msg: message);
    }
else
    {
      controller.reverse();
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
        timeInSecForIos: 3,
        backgroundColor: meatUpTheme,);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(milliseconds: 100), () {
      fetchData();
      fetchBasket();
    });
  }

  @override
  Widget build(BuildContext context) {


    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;

    amount = (_quantity * price);
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    return
      Scaffold(

backgroundColor: backgroundColor,
        body:
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 35,
              ),
              Stack(
                children: <Widget>[
                  Hero(
                    tag: 'hello'+ id.toString(),
                    child:
                    Container(
                      height: deviceHeight/4,
                      width:deviceWidth ,
                      child:
                      Image(
                        image:
                        CachedNetworkImageProvider(image,
                        ), fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap:() {
                      Navigator.pop(context);
                    },
                    child:
                    Container(
                      height: 60,
                      width: 60,
                      margin: EdgeInsets.only(top: 30),
                      child:
                      Image.asset(
                        'images/back.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],

              ),
              Container(
                padding: EdgeInsets.all(20),
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          title != null?  title: '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),

                        Text(
                          price != null? '\u{20A6}' + price.toString(): '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ],
                    ),

                    Text(
                    description != null?  description: "",  style: TextStyle(
                        color: Colors.black45, fontSize: 16),   ),

                    Divider(

                    ),
                    Text(

                      'Select Order Details',style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w500, fontSize: 12 ),
                    ),

                  ],
                ),
              ),



              Container(
                color: Color(0xFFE4E4E4),
                padding: EdgeInsets.all(20),
                child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                              text: 'Quantity',
                              style: TextStyle(
                                  color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16 ),
                              children: [
                                TextSpan(
                                    text: ' *',
                                    style: TextStyle(
                                      color: Colors.red,
                                    ))
                              ]),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),

              Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                        Container(
                          width: 60.0,
                          height: 60.0,
                          child:
                          new FloatingActionButton(
                            heroTag:'minus',
                            onPressed: minus,
                            elevation: 0,
                            child: new Icon(
                                const IconData(
                                    0xe15b, fontFamily: 'MaterialIcons'),
                                color: Colors.black),
                            backgroundColor: Colors.white,),
                        ),
                        SizedBox(height: 10, width: 30,),
                        new Text('$_quantity',
                            style: new TextStyle(fontSize: 18.0)
                        ),
                        SizedBox(height: 10, width: 30,),
                        Container(
                          width: 60.0,
                          height: 60.0,
                          child: new FloatingActionButton(
                            heroTag:'add',
                            onPressed: add,
                            elevation: 0,
                            child: new Icon(
                              Icons.add, color: Colors.black,),
                            backgroundColor: Colors.white,),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                  ]
              ),

              Container(
                color: Color(0xFFE4E4E4),
                padding: EdgeInsets.all(20),
                child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                              text: 'Meat Cut Size',
                              style: TextStyle(
                                  color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16 ),
                              children: [
                                TextSpan(
                                    text: ' *',
                                    style: TextStyle(
                                      color: Colors.red,
                                    ))
                              ]),
                        ),


                      ],
                    ),
                  ],
                ),
              ),

              Flex(direction: Axis.horizontal, children: [
                Expanded(
                  child: FutureBuilder<List<Cut>>(
                      future:   fetchCut(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return Center(
                              child: CircularProgressIndicator());
                        print(snapshot.data);

                        return Column(
                          //scrollDirection: Axis.vertical,
                          //shrinkWrap: true,
                            children: snapshot.data
                                .map(
                                  (feed) => RadioListTile(
                                value: feed.name,
                                activeColor: meatUpTheme,
                                groupValue: cutSelected,
                                onChanged: _valChange,
                                title: Text(
                                  feed.name,
                                  style: TextStyle(fontSize: 16),
                                  //textAlign: TextAlign.center,
                                ),
                              ),
                            )
                                .toList());
                      }),
                ),
              ])

            ],
          ),
        ),
          bottomNavigationBar:
          Container(
            padding: EdgeInsets.all(20),
            height: 90,
            width: deviceWidth,
            child: ProgressButton(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              color: meatUpTheme,
              strokeWidth: 2,
              child:
              Row(
                children: <Widget>[


                  Container(
                    height: 20,
                    width: 20,
                    child:
                    Image.asset('images/addcart.png',
                    ),
                  ),

                  SizedBox(
                    width: 100,
                  ),

              Text(
                "Add $_quantity to Basket(\u{20A6}$amount)",
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              ),

                ],
              ),

                onPressed: (AnimationController controller) {
                if (cutSelected != null){
                  cutSelected != null ?
                  createOrder(context, 1, controller,):
                  Fluttertoast.showToast(msg: 'Quantity can\'t be 0');
                }else{
                  Fluttertoast.showToast(msg: 'Select meat cut size');
                }
              },
            ),
          ),
      );
  }
}