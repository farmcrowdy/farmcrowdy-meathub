import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/MeatPojo.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/colorconst.dart';
import 'package:meathub/meatSelect/meatshare.dart';
import 'package:http/http.dart' as http;
import 'package:meathub/meatSelect/retail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';

class FullOrder extends StatefulWidget {
  final String id;
  FullOrder({Key key, this.id}) : super(key: key);

  @override
  _AlmostThere createState() => _AlmostThere(id);
}

class _AlmostThere extends State<FullOrder> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String id;
  _AlmostThere(this.id);
  var data;
  Map<String, dynamic> responseData;
  var token;
  String name;
  String title;
  String description;

  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
  }

  Future<List<Meats>> _fetchData() async {
    final response = await http.get(
      baseUrl + 'package/type/$id/limit/',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Meats> listOfUsers = items.map<Meats>((json) {
        return Meats.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }
  Future<List<Meats>> toggleId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        toggleId = _fetchData();
      });
    });

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;

    if (id == '2'){
      title = 'Retail order';
      description = 'You can buy your meat based on your preference which can be in bit and pieces.';
    }
    else if ( id == '1'){
      title = 'Meat Share order';
      description = 'You can buy your meat based on your preference which can be in bulk';
    }
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: deviceHeight/4,
                  width:deviceWidth ,
                  child:
                  Image.asset(
                    id == '2'?
                    'images/retailMore.png': 'images/meatshareMore.png' ,fit: BoxFit.fill,),

                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 60,
                    width: 60,
                    margin: EdgeInsets.only(top: 30),
                    // padding: EdgeInsets.only(top: 20,),
                    child: Image.asset(
                      'images/back.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                      description,
                     style: TextStyle(color: Colors.black45, fontSize: 16),
                  ),
                  Divider(),
                  Text(
                    'Available package options',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500, fontSize: 12),
                  ),
                ],
              ),
            ),
            Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  child: FutureBuilder<List<Meats>>(
                      future: toggleId,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return
                            Center(
                              child:
                              Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: deviceHeight/7,
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 40,
                                      padding: EdgeInsets.symmetric(horizontal: 24.0),
                                      child:
                                      Hero(
                                        tag: 'button',
                                        child:
                                        LiquidLinearProgressIndicator(
                                        value: _animationController.value,
                                        backgroundColor: Colors.white,
                                        valueColor: AlwaysStoppedAnimation(meatUpTheme),
                                        borderRadius: 12.0,
                                        center: Text(
                                          "${percentage.toStringAsFixed(0)}%",
                                          style: TextStyle(
                                            color: Colors.redAccent,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    ),
                                  ]
                          ),);
                        return  Container(
                            height: deviceHeight/1.7,
                            child:
                         ListView(
                          //scrollDirection: Axis.vertical,
                          //shrinkWrap: true,
                          children: snapshot.data
                              .map(
                                (feed) => InkWell(
                                  onTap: () {
                                    if(id =='2'){
                                      gotoMeatShare
                                        (context, id: feed.id,
                                          title: feed.name,
                                        image: feed.image
                                      );
                                    }
                                    else if(id =='1'){
                                      gotoMeatShare
                                        (context, id: feed.packageId,
                                          title: feed.name,
                                      image: feed.image);
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(left: 20, top: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                    Hero(
                                    tag: 'hello'+ feed.id.toString(),
                                    child:
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: 10.0),
                                      height: 100.0,
                                      width: 100.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image:
                                            CachedNetworkImageProvider(feed.image),

                                            fit: BoxFit.cover),
                                        borderRadius:
                                        BorderRadius.circular(
                                            12.0),
                                      ),
                                    ),
                                    ),
                                            Flexible(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  '${feed.name}',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 18.0,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  feed.description,
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.black,
                                                  ),
                                                ),

                                              ],
                                            )),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),
                                ),
                              ).toList(),
                         )
                        );
                      }),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  gotoRetail(BuildContext context, {int id, String title, String description}) {
    Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
        pageBuilder: (_,__,___) => RetailOrder(id: id, title: title)
    ));
  }
  gotoMeatShare(BuildContext context, {int id, String title, String image}) {
    Navigator.push(context,PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
        pageBuilder: (_,__,___) =>MeatShareOrder(id: id, title: title, image: image,)
    ));
  }
}
