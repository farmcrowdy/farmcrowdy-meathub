import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/basket/cart.dart';
import 'package:meathub/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicator_button/progress_button.dart';

class RetailOrder extends StatefulWidget {
  final int id;
  final String title;
  RetailOrder({Key key, this.id, this.title}) : super(key: key);
  @override
  _RetailOrder createState() => _RetailOrder(id, title);
}

class _RetailOrder extends State<RetailOrder> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int id;
  String title;
  _RetailOrder(this.id, this.title);
  int _weightVal = 0;
  String description;
  var price = 0;
  String location;
  var locations = List();
  var data;
  String message;
  Map<String, dynamic> responseData;
  var token;
  var userId;
  var orderType;
  var amount= 0;
  var basketNum;

  void _weightValChange(int value) {
    setState(() {
      _weightVal = value;
    });
  }

  int _quantity = 0;
  void add() {
    setState(() {
      _quantity++;
    });
  }

  void minus() {
    setState(() {
      if (_quantity != 0) _quantity--;
    });
  }
void cal(){

}

// get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      userId = prefs.getString('userId');
    });
  }
  // get  the basket data
  fetchBasket() async {
    final response = await http.get(
      baseUrl + 'package/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      setState(() {
        description = data['description'];
        price = data['price'];
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }


//Get basket Number
  fetchBas() async {
    final response = await http.get(
      baseUrl + 'basket/size',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      basketNum = responseData['data'];
      setState(() {
        basketSize = basketNum;
      });
      print(basketSize);
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      setState(() {
        //basketSize = 0;
      });
      throw Exception('Failed to load internet');
    }
  }

  // get all the user data
  fetchData() async {
    print(id);
    final response = await http.get(
      baseUrl + 'package/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print('DATA$data');
      setState(() {
        description = data['description'];
        price = data['price'];
        orderType = data['order_type_id'];

      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }
// get all the cart data
  void fetchCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      subTotal = responseData['data']['subtotal'];
      total = responseData['data']['total'];
      containerCharge = responseData['data']['container_charges'];
      vat = responseData['data']['vat'];
      basketSize = responseData['data']['basket_size'];


    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  //Api to add to the basket
  createOrder( BuildContext context, int type, AnimationController controller)async {
    controller.forward();
    if (_quantity != 0 && _weightVal != 0) {
      var body = {
        'user_id': userId.toString(),
        'package_id': id.toString(),
        'quantity': _quantity.toString(),
        'weight': _weightVal.toString(),
        'order_type_id': orderType.toString(),
        'status_id': '3'
      };
      var header =
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      print(body);
      print(header);
      final response = await http.post(
          baseUrl + 'order/create',
          headers: header, body: body);
      if (response.statusCode == 200) {
        controller.reverse();
        fetchCart();
        fetchBasket();
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(body);
        print(message);
        fetchBas();
        Fluttertoast.showToast(msg: message);
        Navigator.pushReplacementNamed(context, '/BottomNav');
      //  Navigator.pop(context);
      }
else
      {
        controller.reverse();
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIos: 3,
          backgroundColor: meatUpTheme,);
      }
    } else {
      controller.reverse();
      Fluttertoast.showToast(msg: 'Weight and Quantity cannot be empty',
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIos: 3,
        backgroundColor: meatUpTheme,);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(milliseconds: 100), () {
      fetchData();
    });
  }

  @override
  Widget build(BuildContext context) {

    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;


    amount = (_quantity * price * _weightVal);

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[

          Hero(
          tag: 'hello'+ id.toString(),
          child:
          Container(
            height: deviceHeight/4,
            width:deviceWidth ,
            child:
            Image.asset('images/meatshareBasket.png',fit: BoxFit.fill,),

          ),
          ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 60,
                      width: 60,
                      margin: EdgeInsets.only(top: 30),
                      // padding: EdgeInsets.only(top: 20,),
                      child: Image.asset(
                        'images/back.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          title != null?  title: '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),

                        Text(
                          price != null? 'N ' + price.toString(): '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ],
                    ),

                    Text(
                    description !=null ?  description: '',
                        style: TextStyle(color: Colors.black45, fontSize: 16),
                    ),
                    Divider(),
                    Text(
                      'Select Order Details',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),

                Container(
                    height: 60,
                    color: Color(0xFFE4E4E4),
                   //0xFFE4E4E4 padding: EdgeInsets.only(left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              RichText(
                                text: TextSpan(
                                    text: 'Weight',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                    children: [
                                      TextSpan(
                                          text: ' *',
                                          style: TextStyle(
                                            color: Colors.red,
                                          ))
                                    ]),
                              ),

    ],
                        ),
                        ),
                      ],
                    ),
                  ),

                      new RadioListTile<int>(
                        value: 1,
                         activeColor: meatUpTheme,
                        groupValue: _weightVal,
                        onChanged: _weightValChange,
                        title:

                        const Text('1kg'),
                      ),
                      new RadioListTile<int>(
                        value: 2,
                        activeColor: meatUpTheme,
                        groupValue: _weightVal,
                        onChanged: _weightValChange,
                        title: const Text('2kg'),
                      ),

                      new RadioListTile<int>(
                        value: 5,
                        activeColor: meatUpTheme,
                        groupValue: _weightVal,
                        onChanged: _weightValChange,
                        title: const Text('5kg'),
                      ),


              Container(
                color: Color(0xFFE4E4E4),
                height: 60,
                padding: EdgeInsets.only(left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10, top: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                                text: 'Quantity',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                                children: [
                                  TextSpan(
                                      text: ' *',
                                      style: TextStyle(
                                        color: Colors.red,
                                      ))
                                ]),
                          ),
//                          Text(
//                            'Required',
//                            style:
//                                TextStyle(color: Colors.black45, fontSize: 12),
//                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 60.0,
                          height: 60.0,
                          child: FloatingActionButton(
                            heroTag: 'minus',
                            elevation: 0,
                            onPressed: minus,
                            child: new Icon(
                                const IconData(0xe15b,
                                    fontFamily: 'MaterialIcons'),
                                color: Colors.black),
                            backgroundColor: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                          width: 30,
                        ),
                        new Text('$_quantity',
                            style: new TextStyle(fontSize: 18.0)),
                        SizedBox(
                          height: 10,
                          width: 30,
                        ),
                        Container(
                          width: 60.0,
                          height: 60.0,
                          child: FloatingActionButton(
                            heroTag: 'add',
                            elevation: 0,
                            onPressed: add,
                            child: new Icon(
                              Icons.add,
                              color: Colors.black,
                            ),
                            backgroundColor: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    )
                  ]),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.all(20),
          height: 90,
          width: deviceWidth,
          child: ProgressButton(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: meatUpTheme,
            strokeWidth: 2,
            child:  Row(
             ////// mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
Container(
  height: 20,
  width: 20,
  child:
  Image.asset('images/addcart.png',
  ),
),



                SizedBox(
                  width: 90,
                ),

                Text(
                  "Add $_quantity to Basket (N$amount)",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 12.0,
                    color: Colors.white,
                  ),
                ),

              ],
            ),

            onPressed: (AnimationController controller) {

              createOrder(context, 1, controller,);
            },
          ),
        ),
       );
  }
}
