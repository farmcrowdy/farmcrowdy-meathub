import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResetPassField extends StatefulWidget {
  final String phoneNo;

  ResetPassField({Key key, this.phoneNo}) : super(key: key);
  @override
  _Pickup createState() => _Pickup(phoneNo);
}

class _Pickup extends State<ResetPassField> {
  _Pickup(this.phoneNo);
  final String phoneNo;
  TextEditingController oldPasswordController = TextEditingController();
  Map<String, dynamic> responseData;
  var locations = List();
  String _locationId;
  String message;
  var data;
  var token;
  bool passwordVisible;
// Retrieve the token from the shared preference

  changePassword() async {
      var body = {
        'phone_number': phoneNo.toString(),
        'password': oldPasswordController.text,
      };

      var header =
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };

      print(body);
      print(header);
      final response = await http.post(
          baseUrl + 'password/reset/update',
          headers:header, body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(body);
        print(message);
        Fluttertoast.showToast(msg: message);
        oldPasswordController.clear();
        Navigator.pushReplacementNamed(context, "/Login");
      }
      else
      {
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIos: 3,
          backgroundColor: meatUpTheme,);
      }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {

    return
      Scaffold(

          backgroundColor: backgroundColor,
          body:
          SingleChildScrollView(
            child:

            Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top:60),
                  child:
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            children: <Widget>[

SizedBox(
  width: 20,
),
                              Text(
                                'Reset Password',
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20 ),
                              ),
                            ]
                        ),

                        Divider(),
                        Column(

                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(20),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Your New Password',
                                    style: TextStyle(
                                        color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),),
                                  TextFormField(
                                    //controller: emailController,
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          // Based on passwordVisible state choose the icon
                                          passwordVisible
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color:  Colors.black,
                                        ),
                                        onPressed: () {
                                          // Update the state i.e. toogle the state of passwordVisible variable
                                          setState(() {
                                            passwordVisible = !passwordVisible;
                                          });
                                        },
                                      ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                      hintText: 'Enter your new password',

                                      hintStyle:TextStyle(
                                        color: Colors.black45,
                                      ) ,
                                      labelStyle: TextStyle(color: Colors.blue),
                                      border: new OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    obscureText: passwordVisible,
                                    keyboardType: TextInputType.visiblePassword,
                                    style: TextStyle(color: Colors.black),
                                    cursorColor: Colors.black,
                                    controller: oldPasswordController,
                                  ),

                                ],

                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child:
                              Material(
                                  borderRadius: BorderRadius.circular(7.0),
                                  color: meatUpTheme,
                                  elevation: 10.0,
                                  shadowColor: Colors.white70,
                                  child: MaterialButton(
                                    onPressed: (){
                                      changePassword();
                                    },
                                    child:
                                    Text(
                                      'Reset Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 12.0,
                                        color: Colors.white,
                                      ),
                                    ),

                                  )

                              ),

                            )
                          ],
                        )
                      ]
                  ),
                ),
              ],

            ),
          ),




      );
  }
}