
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/colorconst.dart';
import 'package:shared_preferences/shared_preferences.dart';

String universalToken;
class AnimatedSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;
  var token;

  AnimationController animationController;
  Animation<double> animation;
  String home='/BottomNav';
  String login='/LandingPage';
  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }
  void navigationPage() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString('token');
setState(() {
  universalToken = token;
});


    print(token);

    token == null?
    Navigator.of(context).pushReplacementNamed(login):
   Navigator.of(context).pushReplacementNamed(home);
  }
  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 2));
    animation =
    new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: meatUpTheme,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[

          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 54,
                width: 253,
                child:
              new Image.asset(
                'images/meathublogo.png',
              ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}



