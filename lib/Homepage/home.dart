import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/MeatPojo.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/basket/cart.dart';
import 'package:meathub/meatSelect/fullOrder.dart';
import 'package:meathub/meatSelect/meatshare.dart';
import 'package:meathub/meatSelect/retail.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:meathub/colorconst.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
//ListWheelScrollView

class _MyStatefulWidgetState extends State<HomePage> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  var data;

  Map<String, dynamic> responseData;
  var token;
  String name;
  String id = '2';
// get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      name = prefs.getString('name');
    });
  }
  // get the last basket number
// get all the cart data
  void fetchCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      print('hello');
      responseData = json.decode(response.body);

        subTotal = responseData['data']['subtotal'];
        total = responseData['data']['total'];
        containerCharge = responseData['data']['container_charges'];
        vat = responseData['data']['vat'];
        basketSize = responseData['data']['basket_size'];


    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }


  Future<List<Meats>> _fetch3() async {

    final response = await http.get(
      baseUrl + 'package/type/$id/limit/4',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      fetchCart();
      final items = data.cast<Map<String, dynamic>>();
      List<Meats> listOfUsers = items.map<Meats>((json) {
        return Meats.fromJson(json);
      }).toList();
      return listOfUsers;
    }

    else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  Future<List<Meats>> toggleId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        toggleId = _fetch3();

      });
    });


    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {
    }));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;
    final percentage = _animationController.value * 100;
    return Scaffold(
        backgroundColor: backgroundColor,
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  //alignment: Alignment.topRight,
                  children: <Widget>[
                    Container(

                      child: Image.asset(
                        'images/home.png',
                        width: deviceWidth,
                        height:220,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 200,
                          // height: deviceHeight/3.6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ToggleSwitch(
                              minWidth: 150.0,
                              initialLabelIndex: 0,
                              activeBgColor: Colors.white,
                              activeTextColor: meatUpTheme,
                              inactiveBgColor: Color(0xffE3E3E1),
                              inactiveTextColor: Color(0xff9F9F9C),
                              labels: ['Retail', 'MeatShare'],
                              onToggle: (index) {
                                print('switched to: $index');
                                if (index == 0) {
                                  id = '2';
                                  setState(() {
                                    toggleId = _fetch3();
                                  });
                                } else if (index == 1) {
                                  id = '1';
                                  setState(() {
                                    toggleId = _fetch3();
                                  });
                                  _fetch3();
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        name != null?
                        'Hi $name': '',
                        style: GoogleFonts.googleSans(
                          textStyle: TextStyle(
                            color: Colors.black45,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        'What are you getting today?',
                        style: GoogleFonts.googleSans(
                          textStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Flex(direction: Axis.horizontal, children: [
                  Expanded(
                    child: FutureBuilder<List<Meats>>(
                        future: toggleId,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData)
                            return  Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: deviceHeight/7,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    height: 40,
                                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                                    child: LiquidLinearProgressIndicator(
                                      value: _animationController.value,
                                      backgroundColor: Colors.white,
                                      valueColor: AlwaysStoppedAnimation(meatUpTheme),
                                      borderRadius: 12.0,
                                      center: Text(
                                        "${percentage.toStringAsFixed(0)}%",
                                        style: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),

                                ]

                            );
                          return
                            Container(
                              height: deviceHeight/2,
                              child:
                              ListView(
                            //scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            children: snapshot.data
                                .map(
                                  (feed) => InkWell(
                                    onTap: () {
                                      if(id =='2'){
                                        gotoMeatShare
                                          (context, id: feed.id,
                                            title: feed.name,
                                        image: feed.image);
                                      }
                                      else if(id =='1'){
                                        gotoMeatShare
                                          (context, id: feed.id,
                                            title: feed.name,
                                            image: feed.image);
                                      }
                                      },

                                    child:
                                      Container(
                                      padding: EdgeInsets.only(left: 20, top: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                      Hero(
                                      tag: 'hello' + feed.id.toString(),
                                        child:
                                              Container(
                                                margin: EdgeInsets.only(
                                                    right: 10.0),
                                                height: 100.0,
                                                width: 100.0,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                        image:
                                                        CachedNetworkImageProvider(
                                                            feed.image != null ?
                                                            feed.image: ''),

                                                      fit: BoxFit.cover),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12.0),
                                                ),
                                              ),
                                      ),

                                              Flexible(
                                                  child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    feed.name != null ?
                                                    '${feed.name}': '',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      fontSize: 18.0,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    feed.description != null ?
                                                    feed.description: '',
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),

                                                ],
                                              )),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                              ),
                          );
                        }),
                  ),
                ]),
              ],
            ),
          ],
        ),
        bottomNavigationBar:  Container(
      width: deviceWidth,
      padding: EdgeInsets.all(30),
      child:   Hero(
        tag: 'button',
        child:Material(
            borderRadius: BorderRadius.circular(7.0),
            color: meatUpTheme,
            elevation: 10.0,
            shadowColor: Colors.white70,
            child: MaterialButton(
              onPressed: () {
                viewMore(context, id: id);
              },
              child: Text(
                'View More',
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              ),
            )),
      ),
    ),
    );
  }

      gotoRetail(BuildContext context, {int id, String title, String description}) {
        Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
        pageBuilder: (_,__,___) => RetailOrder(id: id, title: title)
        ));
      }

      gotoMeatShare(BuildContext context, {int id, String title, String image}) {
        Navigator.push(context,PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
        pageBuilder: (_,__,___) =>MeatShareOrder(id: id, title: title, image:image)
        ));
      }

  viewMore(BuildContext context, {String id}) {
    Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (_,__,___) =>
     FullOrder(id: id)));
    }
  }

