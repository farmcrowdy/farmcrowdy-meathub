import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Homepage/home.dart';
import 'package:meathub/Profile/profile.dart';
import 'package:meathub/basket/cart.dart';
import 'package:meathub/colorconst.dart';
import 'package:meathub/order/order.dart';
import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';
import 'package:animations/animations.dart';

class BottomNav extends StatefulWidget {

  BottomNav({Key key}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
//ListWheelScrollView

class _MyStatefulWidgetState extends State<BottomNav> {

  List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
      icon:
      ImageIcon(
        AssetImage('images/homeicon.png',),
      ),
      title: Text('Home'),
    ),
    BottomNavigationBarItem(
      icon:
      ImageIcon(
        AssetImage('images/carticon.png'),
      ),
      title: Text('Basket'),
    ),
    BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage('images/stackicon.png'),
      ),
      title: Text('Order'),
    ),
    BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage('images/profileIcon.png'),
      ),
      title: Text('Profile'),
    ),
  ];


  int _selectedIndex = 0;
  BottomNavigationBadge badger = new BottomNavigationBadge(
      backgroundColor: meatUpTheme,
      badgeShape: BottomNavigationBadgeShape.circle,
      textColor: Colors.yellow,
      position: BottomNavigationBadgePosition.topRight,
      textSize: 6);
  List<Widget> pageList = <Widget>[

  HomePage(),
  Cart(),
  Order(),
    Profile(),
  ];

@override
  void initState() {
    // TODO: implement initState
 // badge();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    setState(() {
      items = badger.setBadge(items,  basketSize.toString(), 1);
    });

    return Scaffold(
backgroundColor: backgroundColor,
      body:
      PageTransitionSwitcher(
        transitionBuilder: (
            Widget child,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            ) {
          return FadeThroughTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: pageList[_selectedIndex],
      ),
      bottomNavigationBar:
     BottomNavigationBar(
       type: BottomNavigationBarType.fixed,
        onTap:(newIndex) => setState(() => _selectedIndex = newIndex),
        currentIndex: _selectedIndex,
        items:items,
        selectedItemColor: meatUpTheme,
      ),

    );
  }
}