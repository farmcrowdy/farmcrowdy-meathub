import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/Homepage/bottmNav.dart';
import 'package:meathub/Homepage/home.dart';
import 'package:meathub/Profile/profile.dart';
import 'package:meathub/Profile/security.dart';
import 'package:meathub/Profile/yourDetails.dart';
import 'package:meathub/basket/cart.dart';
import 'package:meathub/basket/pickup.dart';
import 'package:meathub/forgotPassword/forgotpassword.dart';
import 'package:meathub/forgotPassword/security.dart';
import 'package:meathub/login/loginpage.dart';
import 'package:meathub/meatSelect/fullOrder.dart';
import 'package:meathub/meatSelect/meatshare.dart';
import 'package:meathub/meatSelect/retail.dart';
import 'package:meathub/onBoardingScreen/onBoard_scaffold.dart';
import 'package:meathub/order/order.dart';
import 'package:meathub/order/orderDetails.dart';
import 'package:meathub/signup/almostThere.dart';
import 'package:meathub/signup/signup.dart';
import 'package:meathub/signup/verification.dart';
import 'package:meathub/splashscreen.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fluttertoast/fluttertoast.dart';



void main() => runApp(MyApp());


  class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
  return _MyAppState();
  }
  }
  class _MyAppState extends State<MyApp> {
    var basketNum;
    var data;
    Map<String, dynamic> responseData;
    var token;
    String name;
    // get the token for the user
    void getToken() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        token = prefs.getString('token');
      });
      print('object');
    }
// get all the cart data
    void fetchCart() async {
      print('fndfhf');
      final response = await http.get(
        baseUrl + 'basket/summary',
        headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        setState(() {
        subTotal = responseData['data']['subtotal'];
        total = responseData['data']['total'];
        containerCharge = responseData['data']['container_charges'];
        vat = responseData['data']['vat'];
        basketSize = responseData['data']['basket_size'];
        });

      } else {
        var sa = response.body;
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        print(sa);
        throw Exception('Failed to load internet');
      }
    }

//Get basket Number
    fetchData() async {
      final response = await http.get(
        baseUrl + 'basket/size',
        headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        basketNum = responseData['data'];
        setState(() {
          basketSize = basketNum;
        });
        print(basketSize);
      } else {
        var sa = response.body;
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        print(sa);
        //throw Exception('Failed to load internet');
        setState(() {
          basketSize = 0;
        });
      }
    }

    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    @override
    void initState() {
      super.initState();
      getToken();
      Timer(Duration(milliseconds: 1000), () {
        setState(() {
//          fetchData();
          fetchCart();

        });
      });
      var initializationSettingsAndroid = new AndroidInitializationSettings('app_icon');

      var initializationSettingsIOS = new IOSInitializationSettings(
          onDidReceiveLocalNotification: onDidRecieveLocalNotification);

      var initializationSettings = new InitializationSettings(
          initializationSettingsAndroid, initializationSettingsIOS);

      flutterLocalNotificationsPlugin.initialize(initializationSettings,
          onSelectNotification: onSelectNotification);

      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) {
          print('on message ${message}');
          // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
          displayNotification(message);
          // _showItemDialog(message);
        },
        onResume: (Map<String, dynamic> message) {
          print('on resume $message');
        },
        onLaunch: (Map<String, dynamic> message) {
          print('on launch $message');
        },
      );
      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
      _firebaseMessaging.getToken().then((String token) {
        assert(token != null);
        print(token);
      });
    }

    Future displayNotification(Map<String, dynamic> message) async{
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'channelid', 'flutterfcm', 'your channel description',
          importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platformChannelSpecifics,
        payload: 'hello',);
    }
    Future onSelectNotification(String payload) async {
      if (payload != null) {
        debugPrint('notification payload: ' + payload);
      }
      await Fluttertoast.showToast(
          msg: "Notification Clicked",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.black54,
          textColor: Colors.white,
          fontSize: 16.0
      );
      /*Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
    );*/
    }

    Future onDidRecieveLocalNotification(
        int id, String title, String body, String payload) async {
      // display a dialog with the notification details, tap ok to go to another page
      showDialog(
        context: context,
        builder: (BuildContext context) => new CupertinoAlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: [
            CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
                await Fluttertoast.showToast(
                    msg: "Notification Clicked",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.black54,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
              },
            ),
          ],
        ),
      );
    }



    @override
    Widget build(BuildContext context) {
      return MaterialApp(
        theme: ThemeData(fontFamily: 'Gordita'),
          debugShowCheckedModeBanner: false,
          title: 'Meathub',
          home: AnimatedSplashScreen(),
    routes: <String,WidgetBuilder>{
    "/LandingPage":(BuildContext context)=>new LandingPage(),
    "/SignUp":(BuildContext context)=>new SignUp(),
      "/VerifyPin":(BuildContext context)=>new VerifyPin(),
      "/AlmostThere":(BuildContext context)=>new AlmostThere(),
      "/Login":(BuildContext context)=>new Login(),
      "/HomePage":(BuildContext context)=>new HomePage(),
      "/RetailOrder":(BuildContext context)=>new RetailOrder(),
      "/MeatShareOrder":(BuildContext context)=>new MeatShareOrder(),
      "/BottomNav":(BuildContext context)=>new BottomNav(),
      "/Pickup":(BuildContext context)=>new Pickup(),
      "/Order":(BuildContext context)=>new Order(),
      "/OrderDetails":(BuildContext context)=>new OrderDetails(),
      "/FullOrder":(BuildContext context)=>new FullOrder(),
      "/Cart":(BuildContext context)=>new Cart(),
      "/Profile":(BuildContext context)=>new Profile(),
      "/ProfileDetails":(BuildContext context)=>new ProfileDetails(),
      "/Security":(BuildContext context)=>new Security(),
      "/MyApp":(BuildContext context)=>new MyApp(),
      "/ResetPassword":(BuildContext context)=>new ResetPassword(),
      "/ResetPassField":(BuildContext context)=>new ResetPassField(),
    },
      );
  }
  }