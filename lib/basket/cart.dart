import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/basketOrderPojo.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/basket/pickup.dart';
import 'package:meathub/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';

var basketSize;
var subTotal;
var total;
var containerCharge;
var vat;

var token;

String message;
var weight;
var res;

class Cart extends StatefulWidget {
  @override
  Carts createState() => Carts();
}

class Carts extends State<Cart> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final GlobalKey<Carts> _key = GlobalKey();

  var weight;
  var _quantity;
  var orderType;
  var id;
  var price;
  String name;
  var data;
  var totalCharge;
  Map<String, dynamic> responseData;

// get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
  }

  // get all the cart data
  void fetchData() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      subTotal = responseData['data']['subtotal'];
      total = responseData['data']['total'];
      basketSize = responseData['data']['basket_size'];
      totalCharge = (total * 0.015) + 100;
      if( totalCharge > 2000 ){
        totalCharge = 2000;
      }
      else if( total == 0 ){
        totalCharge = 0;
      }

    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);

      throw Exception('Failed to load internet');
    }
  }

  Future<List<Basket>> _fetch3() async {
    fetchData();
    final response = await http.get(
      baseUrl + 'basket/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data']['orders'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Basket> listOfUsers = items.map<Basket>((json) {
        return Basket.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  Future<List<Basket>> refresh;

  Future<String> getData() async {
    fetchData();
    var response = await http.get(
      baseUrl + 'basket/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    this.setState(() {
      res = json.decode(response.body);
      data = res['data']['orders'];
      print(data.toString());
      //print(res);
      setState(() {
//        _quantity = data['quantity'];
//        weight = data['weight'];
//        orderType = data['order_type_id'];
//        price = data['price'];
//        id = data['id'];
//        name = data['name'];
      });
      print(id);
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();

    Timer(Duration(milliseconds: 100), () {
      setState(() {
        fetchData();
        refresh = _fetch3();
        // fetchData();
      });
    });
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void add() {
    setState(() {
      _quantity++;
    });
  }

  void minus() {
    setState(() {
      if (_quantity != 0) _quantity--;
    });
  }

  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery.of(context).size.width;

    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 60, left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
//                        GestureDetector(
//                          onTap: () {
//                            Navigator.pop(context);
//                          },
//                          child: Container(
//                            height: 60,
//                            width: 60,
//                            //margin: EdgeInsets.only(top: 20),
//                            // padding: EdgeInsets.only(top: 20,),
//                            child: Image.asset(
//                              'images/back.png',
//                              fit: BoxFit.fill,
//                            ),
//                          ),
//                        ),

                        Text(
                          'Basket',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(basketSize != null ? '($basketSize items)' : '',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.w400))
                      ],
                    ),
                    Divider(),
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      child: Text(
                        'Select pick-up location to checkout successfully',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 12),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 6,
                color: Color(0xFFE7E7E7),
                padding: EdgeInsets.all(2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[],
                ),
              ),
              Flex(direction: Axis.horizontal, children: [
                Expanded(
                  child: FutureBuilder<List<Basket>>(
                      future: refresh,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Column(children: <Widget>[
                            SizedBox(
                              height: 300,
                            ),
                            Container(
                              width: double.infinity,
                              height: 40,
                              padding: EdgeInsets.symmetric(horizontal: 24.0),
                              child: LiquidLinearProgressIndicator(
                                value: _animationController.value,
                                backgroundColor: Colors.white,
                                valueColor: AlwaysStoppedAnimation(meatUpTheme),
                                borderRadius: 12.0,
                                center: Text(
                                  "${percentage.toStringAsFixed(0)}%",
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ]);
                        }
                        //print(snapshot.data);
                        else if (snapshot.data.isNotEmpty) {
                          return Container(
                            height: deviceHeight / 2.5,
                            child: ListView(
                              //scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: snapshot.data
                                  .map(
                                    (feed) => InkWell(
                                      child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      right: 10.0,
                                                    ),
                                                    height: 120.0,
                                                    width: 110.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image:
                                                              CachedNetworkImageProvider(
                                                                  feed.image),
                                                          fit: BoxFit.cover),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12.0),
                                                    ),
                                                  ),
                                                  Flexible(
                                                      child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                right: 10),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Text(
                                                              feed.name,
                                                              style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w800,
                                                                fontSize: 15.0,
                                                                color: Colors
                                                                    .black,
                                                              ),
                                                            ),
                                                            Text(
                                                              '\u{20A6}' +
                                                                  feed.price
                                                                      .toString(),
                                                              style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w800,
                                                                fontSize: 20.0,
                                                                color: Colors
                                                                    .black,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  right: 10),
                                                          child: feed.orderType ==
                                                                  1
                                                              ? Text('')
                                                              : Text(
                                                                  'Weight: ${feed.weight} kg')),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Container(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  right: 10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              Text(
                                                                  'Quantity: ${feed.quantity} '),
                                                              InkWell(
                                                                onTap: () {
                                                                  //Update the user profile
                                                                  void
                                                                      delete() async {
                                                                    print(
                                                                        'hello');
                                                                    var header =
                                                                        {
                                                                      'Accept':
                                                                          'application/json',
                                                                      'Authorization':
                                                                          'Bearer $token'
                                                                    };
                                                                    print(
                                                                        header);
                                                                    final response =
                                                                        await http
                                                                            .delete(
                                                                      baseUrl +
                                                                          'order/delete/${feed.id}',
                                                                      headers:
                                                                          header,
                                                                    );
                                                                    if (response
                                                                            .statusCode ==
                                                                        200) {
                                                                      setState(
                                                                          () {
                                                                        refresh =
                                                                            _fetch3();
                                                                        setState(
                                                                            () {
                                                                          basketSize =
                                                                              (basketSize - 1);
                                                                        });
                                                                        //fetchData();
                                                                      });
                                                                      responseData =
                                                                          json.decode(
                                                                              response.body);
                                                                      message =
                                                                          responseData[
                                                                              "message"];
                                                                      data = responseData[
                                                                          "data"];
                                                                      print(
                                                                          responseData);
                                                                      print(
                                                                          message);
                                                                      Fluttertoast
                                                                          .showToast(
                                                                              msg: message);
                                                                    }
                                                                    {
                                                                      responseData =
                                                                          json.decode(
                                                                              response.body);
                                                                      var er =
                                                                          responseData[
                                                                              'message'];
                                                                      print(
                                                                          'error$er');
                                                                      Fluttertoast
                                                                          .showToast(
                                                                        msg: er,
                                                                        toastLength:
                                                                            Toast.LENGTH_LONG,
                                                                        timeInSecForIos:
                                                                            3,
                                                                        backgroundColor:
                                                                            meatUpTheme,
                                                                      );
                                                                    }
                                                                  }

                                                                  showDialog(
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (context) {
                                                                        return AlertDialog(
                                                                          title:
                                                                              Text(
                                                                            "Delete item from cart",
                                                                          ),
                                                                          content:
                                                                              Text(
                                                                            "Are you sure you want to delete this item from your cart?",
                                                                          ),
                                                                          actions: <
                                                                              Widget>[
                                                                            FlatButton(
                                                                              child: Text(
                                                                                "Cancel",
                                                                                style: TextStyle(color: Colors.white, fontSize: 16),
                                                                              ),
                                                                              onPressed: () => Navigator.pop(context),
                                                                              color: Colors.grey,
                                                                            ),
                                                                            FlatButton(
                                                                              child: Text(
                                                                                "Delete",
                                                                                style: TextStyle(color: meatUpTheme, fontSize: 16),
                                                                              ),
                                                                              onPressed: () {
                                                                                Navigator.pop(context);
                                                                                delete();
                                                                              },
                                                                            )
                                                                          ],
                                                                        );
                                                                      });
                                                                },
                                                                child:
                                                                    Image.asset(
                                                                  'images/deleteicon.png',
                                                                  fit: BoxFit
                                                                      .fill,
                                                                  height: 20,
                                                                  width: 20,
                                                                ),
                                                              ),
                                                            ],
                                                          )),
                                                    ],
                                                  )),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Divider(),
                                            ],
                                          )),
                                    ),
                                  )
                                  .toList(),
                            ),
                          );
                        } else if (snapshot.hasError) {
                          return Column(
                            children: <Widget>[
                              SizedBox(
                                height: 100,
                              ),
                              Text(
                                'No order yet',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                  "${'Once an order is placed all itemsr can be found here'}"),
                              SizedBox(
                                height: 100,
                              ),
                            ],
                          );
                        } else
                          return Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: 250,
                                ),
                                Text(
                                  'No order yet',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  children: <Widget>[
                                    Text(
                                        "${'Once an order is placed all items'}"),
                                    Text("${'can be found here'}"),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.all(20),
                                  width: deviceWidth,
                                  child: Material(
                                      borderRadius: BorderRadius.circular(7.0),
                                      color: meatUpTheme,
                                      elevation: 10.0,
                                      shadowColor: Colors.white70,
                                      child: MaterialButton(
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, "/BottomNav");
                                        },
                                        child: Text(
                                          'Place Order',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 12.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      )),
                                )
                              ],
                            ),
                          );
                      }),
                ),
              ]),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 6,
                color: Color(0xFFE7E7E7),
                padding: EdgeInsets.all(1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[],
                ),
              ),
              Column(crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20, top: 20),
                      child: Text(
                        'Order summary',
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Sub-total',
                              style: TextStyle(
                                // fontWeight: FontWeight.w800,
                                fontSize: 16.0,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              subTotal != null
                                  ? '\u{20A6}' + subTotal.toString()
                                  : '',
                              style: TextStyle(
                                // fontWeight: FontWeight.w800,
                                fontSize: 16.0,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        )),
                    Container(
                        padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Transaction charges',
                              style: TextStyle(
                                // fontWeight: FontWeight.w800,
                                fontSize: 16.0,
                                color: Colors.black,
                              ),
                            ),

                            Row(
                                children: <Widget>[
                                  new Text('\u{20A6}' ,style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16),),
                                  Text(
                                    totalCharge !=null && total !=null?    totalCharge.round().toString(): '',
                                    style: TextStyle(
                                      // fontWeight: FontWeight.w800,
                                      fontSize: 16.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                ]
                            ),
                          ],
                        )),
                    SizedBox(
                      height: 10,
                    ),
//              Container(
//                  padding: EdgeInsets.only(left: 20, right: 20, top: 10),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(
//                        'Container Charges',
//                        style: TextStyle(
//                          // fontWeight: FontWeight.w800,
//                          fontSize: 16.0,
//                          color: Colors.black,
//                        ),
//                      ),
//                      Text(
//                          containerCharge != null ? 'N'+ containerCharge.toString(): '',
//                        style: TextStyle(
//                          // fontWeight: FontWeight.w800,
//                          fontSize: 16.0,
//                          color: Colors.black,
//                        ),
//                      ),
//                    ],
//                  )),
//              Container(
//                  padding: EdgeInsets.only(left: 20, right: 20, top: 10),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(
//                        'VAT',
//                        style: TextStyle(
//                          // fontWeight: FontWeight.w800,
//                          fontSize: 16.0,
//                          color: Colors.black,
//                        ),
//                      ),
//                      Text(
//                          vat !=null ?   'N' + vat.toString(): '',
//                        style: TextStyle(
//                          // fontWeight: FontWeight.w800,
//                          fontSize: 16.0,
//                          color: Colors.black,
//                        ),
//                      ),
//                    ],
//                  )),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Total',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              totalCharge != null ? '\u{20A6}' + (total+totalCharge.round()).toString() : '',
//                              total != null
//                                  ? '\u{20A6}' + total.toString()
//                                  : '',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        )),
                  ]),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          margin: EdgeInsets.all(20),
          child: Material(
              borderRadius: BorderRadius.circular(7.0),
              color: meatUpTheme,
              elevation: 10.0,
              shadowColor: Colors.white70,
              child: MaterialButton(
                onPressed: () {
                  gotoPickup(
                    context,
                  );
                  //  Navigator.pushNamed(context, "/Pickup");
                },
                child: Text(
                  'Select Pick-up Location',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 12.0,
                    color: Colors.white,
                  ),
                ),
              )),
        ));
  }

  gotoPickup(
    BuildContext context,
  ) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => Pickup()));
  }
}
