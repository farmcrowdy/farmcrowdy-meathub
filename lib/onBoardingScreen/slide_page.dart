import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meathub/colorconst.dart';
import 'package:meathub/onBoardingScreen/slider.dart';

class SlideItem extends StatelessWidget {
  final int index;
  SlideItem(this.index);

  @override
  Widget build(BuildContext context) {
    return Stack( // <-- STACK AS THE SCAFFOLD PARENT
        children: [
    Container(
    decoration: BoxDecoration(
    image: DecorationImage(
      colorFilter: new ColorFilter.mode(meatUpTheme
          .withOpacity(1.0), BlendMode.dstATop),
    image: AssetImage(sliderArrayList[index].sliderImageUrl),// <-- BACKGROUND IMAGE
    fit: BoxFit.fill,
    ),
    ),
    ),
    Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[

        SizedBox(
          height: 200,
        ),
        Text(
          sliderArrayList[index].sliderHeading,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.white
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              sliderArrayList[index].sliderSubHeading,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 16,
                  color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),

          ),

        ),
        SizedBox(
          height: 200.0,
        ),
        Container(
          width: 400,
          height: 44,
          child:Material(
            borderRadius: BorderRadius.circular(7.0),
            color:  Color(0xff5B1212),
            elevation: 10.0,
            child: MaterialButton(
              padding: EdgeInsets.all(14),
                onPressed: (){
                  Navigator.pushReplacementNamed(context, "/SignUp");
                },
                child:
                    Text(
                      'Create your Meathub Account',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 12.0,
                        color: Colors.white,
                      ),
                    ),
                )
        ),
        ),
        SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: () =>  Navigator.pushReplacementNamed(context, "/Login"),
          child:
        Text(
          'Log in',
          style: TextStyle(
            fontSize: 12.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        ),
      ],
    ),
    ],
    );
  }
}
