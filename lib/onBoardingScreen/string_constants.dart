class Constants{
  static const String POPPINS = "Poppins";
  static const String OPEN_SANS = "OpenSans";
  static const String SKIP = "Skip";
  static const String NEXT = "Next";
  static const String SLIDER_HEADING_1 = "Provides Accessibility";
  static const String SLIDER_HEADING_2 = "Affordable pricing";
  static const String SLIDER_HEADING_3 = "Convenience matters";
  static const String SLIDER_DESC = "Our modern processing facility affords us the opportunity to process our meat in a clean environment with a hygenic slaughtering process.";
  static const String SLIDER_DESC_2 = "Our pricing are competitive with the entire meat market. We provide the best, bring it close to home and at an affordable rate.";
  static const String SLIDER_DESC_3 = "You have the convenience of ordering remotely without hassles and picking up at a Meathub location nearest to you.";

}