class Meats {

  String  name;
  String  description;
  int id;
  int packageId;
  String createdAt;
  String updatedAt;
  String image;

  Meats({
    this.name,
    this.description,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.packageId,
    this.image
  });

  Meats.fromJson(Map<String, dynamic> doc):
        name =  doc['name'],
        description =  doc['description'],
        id =  doc['id'],
  updatedAt = doc['updated_at'],
  packageId = doc['order_type_id'],
  createdAt = doc['created_at'],
  image=  doc['image'];


}
