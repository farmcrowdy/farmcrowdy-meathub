import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meathub/Engine/httpDetails.dart';
import 'package:meathub/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Security extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<Security> {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  Map<String, dynamic> responseData;
  var locations = List();
  String _locationId;
  String message;
  var data;
  var token;
  var phoneNo;
  bool passwordVisible;
  bool passwordVisible2;


// Retrieve the token from the shared preference

  void getToken() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      phoneNo = prefs.getString('phoneNo');
    });
  }

  changePassword() async {
      var body = {
        'phone_number': phoneNo.toString(),
        'password': oldPasswordController.text,
        'new_password': newPasswordController.text,
      };

      var header =
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };

      print(body);
      print(header);
      final response = await http.post(
          baseUrl + 'password/update',
          headers:header, body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(body);
        print(message);
        Fluttertoast.showToast(msg: message);
        oldPasswordController.clear();
        newPasswordController.clear();
        Navigator.pop(context);
      }

      {
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIos: 3,
          backgroundColor: meatUpTheme,);
      }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    passwordVisible = true;
    passwordVisible2 = true;
  }

  @override
  Widget build(BuildContext context) {

    return
      Scaffold(

          backgroundColor: backgroundColor,
          body:
          SingleChildScrollView(
            child:

            Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top:60),
                  child:
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            children: <Widget>[

                              GestureDetector(
                                onTap:() {
                                  Navigator.pop(context);
                                },
                                child:
                                Container(
                                  height: 60,
                                  width: 60,
                                  //margin: EdgeInsets.only(top: 20),
                                  // padding: EdgeInsets.only(top: 20,),
                                  child:
                                  Image.asset(
                                    'images/back.png',
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Text(
                                'Security',
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20 ),
                              ),
                            ]
                        ),

                        Divider(),
                        Column(

                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(20),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Current Password',
                                    style: TextStyle(
                                        color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),),
                                  TextFormField(
                                    //controller: emailController,
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          // Based on passwordVisible state choose the icon
                                          passwordVisible
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color:  Colors.black,
                                        ),
                                        onPressed: () {
                                          // Update the state i.e. toogle the state of passwordVisible variable
                                          setState(() {
                                            passwordVisible = !passwordVisible;
                                          });
                                        },
                                      ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                      hintText: 'Enter current password',

                                      hintStyle:TextStyle(
                                        color: Colors.black45,
                                      ) ,
                                      labelStyle: TextStyle(color: Colors.blue),
                                      border: new OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    obscureText: passwordVisible,
                                    keyboardType: TextInputType.visiblePassword,
                                    style: TextStyle(color: Colors.black),
                                    cursorColor: Colors.black,
                                    controller: oldPasswordController,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(20),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'New Password',
                                    style: TextStyle(
                                        color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),),
                                  TextFormField(
                                    //controller: emailController,
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          // Based on passwordVisible state choose the icon
                                          passwordVisible2
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color:  Colors.black,
                                        ),
                                        onPressed: () {
                                          // Update the state i.e. toogle the state of passwordVisible variable
                                          setState(() {
                                            passwordVisible2 = !passwordVisible2;
                                          });
                                        },
                                      ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                      hintText: 'Enter your new password',
                                      hintStyle:TextStyle(
                                        color: Colors.black45,
                                      ) ,
                                      labelStyle: TextStyle(color: Colors.blue),
                                      border: new OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    keyboardType: TextInputType.visiblePassword,
                                    style: TextStyle(color: Colors.black),
                                    cursorColor: Colors.black,
                                    obscureText: passwordVisible2,
                                    controller: newPasswordController,
                                  ),
                                ],
                              ),
                            ),

                            Container(
                              margin: EdgeInsets.all(20),
                              child:
                              Material(
                                  borderRadius: BorderRadius.circular(7.0),
                                  color: meatUpTheme,
                                  elevation: 10.0,
                                  shadowColor: Colors.white70,
                                  child: MaterialButton(
                                    onPressed: (){
                                      changePassword();
                                    },
                                    child:
                                    Text(
                                      'Change Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 12.0,
                                        color: Colors.white,
                                      ),
                                    ),

                                  )

                              ),

                            )
                          ],
                        )
                      ]
                  ),
                ),
              ],
            ),
          ),



      );
  }
}